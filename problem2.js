function lastcardetail(inventory,carid)
{
    if(inventory == undefined)
    {
        return "Define the array";
    }
    if(carid == undefined || inventory.length == 0)
    {
        return [];
    }

    return "Last car is a " + inventory[carid].car_make + " " + inventory[carid].car_model;
         
}
module.exports=lastcardetail;
