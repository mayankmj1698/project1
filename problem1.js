function cardetail(inventory,carid)
{
    if(inventory == undefined)
    {
        return "Define the array";
    }
    if(carid == undefined || inventory.length == 0)
    {
        return [];
    }

    return "Car 33 is a " + inventory[carid].car_year + " " + inventory[carid].car_make + " " + inventory[carid].car_model;
         
}
module.exports=cardetail;
