function sortcarname(inventory)
{
    if(inventory == undefined)
    {
        return "Define the array";
    }
    if(inventory.length == 0)
    {
        return [];
    }

    let carname = [];
    for (let i = 0; i < inventory.length; i++) 
    {
        carname[i] = inventory[i].car_model;
    }
    carname.sort();
    return carname;
}
module.exports=sortcarname;
