function caryear(inventory,carid)
{
    if(inventory == undefined)
    {
        return "Define the array";
    }
    if( inventory.length == 0)
    {
        return [];
    }

    let caryear = [];
        for (let i = 0; i < inventory.length; i++) 
        {
            caryear[i] = inventory[i].car_year;
        }
        
        return caryear;
         
}
module.exports=caryear;
