function oldercar(inventory)
{
    if(inventory == undefined)
    {
        return "Define the array";
    }
    if( inventory.length == 0)
    {
        return [];
    }

    let cars =[];
        let k=0;
        for (let i = 0; i < inventory.length; i++) {
        
            if (inventory[i].car_year < 2000) {
        
                cars[k]=inventory[i].car_year;
                k++;
            }
        }

    return cars
}
module.exports=oldercar;
